import connexion
from connexion import NoContent
import yaml
import requests
import uuid
import logging
import logging.config
import random
from pykafka import KafkaClient
import datetime
import json
from swagger_ui_bundle import swagger_ui_path
import time
import os

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

# def create_trace_id():
#    trace_id = random.randint(1, 999999999)
#    return trace_id

current_retry_num = 0
max_tries = 12
time_in_seconds = 6

while current_retry_num < max_tries:
    try:
        host = str(app_config["events"]["hostname"]) + ':' + str(app_config["events"]["port"])
        client = KafkaClient(hosts=host)
        topic = client.topics[str.encode(app_config["events"]["topic"])]
        producer = topic.get_sync_producer()
        break
    except:
        print("connection refused to kafka client")
        time.sleep(time_in_seconds)
        current_retry_num += 1


def get_health():
    """ gets the health of each service and return 200 """
    logger.info("HEALTH CHECK - Return 200")
    dict = {"message": "running"}
    return dict, 200


def report_carbon_monoxide_reading(body):
    """Receives a carbon monoxide event """
    trace_id = str(uuid.uuid4())
    body["trace_id"] = trace_id

    logger.info(f"Received event carbon monoxide request with a trace id of body{trace_id}")
    # host = str(app_config["events"]["hostname"]) + ':' + str(app_config["events"]["port"])
    # logger.debug(f'hostname: {host}')

    # client = KafkaClient(hosts=host)
    # topic = client.topics[str.encode(app_config['events']['topic'])]
    # producer = topic.get_sync_producer()
    msg = {"type": "carbon_monoxide",
           "datetime": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"),
           "payload": body}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info(f"Returned event carbon monoxide response with trace_id {trace_id} status code")
    return NoContent, 201


def report_nitrogen_dioxide_reading(body):
    """Receives a nitrogen dioxide event """
    trace_id = str(uuid.uuid4())
    # trace = create_trace_id()
    body["trace_id"] = trace_id

    logger.info(f"Received event nitrogen dioxide request with a trace id of body{trace_id}")

    # headers = {"content-type": "application/json"}
    # response = requests.post(app_config['eventstore2']['url'], json=body, headers=headers)
    # logger.info(f"Returned event carbon monoxide response with trace_id {trace} status code and {response.status_code}")
    # return NoContent, response.status_code
    # host = str(app_config["events"]["hostname"]) + ':' + str(app_config["events"]["port"])
    # client = KafkaClient(hosts=host)
    # topic = client.topics[str.encode(app_config['events']['topic'])]
    # producer = topic.get_sync_producer()
    msg = {"type": "nitrogen_dioxide",
           "datetime": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"),
           "payload": body}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info(f"Returned event nitrogen dioxide response with trace_id {trace_id} status code")
    return NoContent, 201


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml", base_path="/receiver", strict_validation=True, validate_responses=True)
# app.add_api("openapi.yaml",
#            strict_validation=True,
#            validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080)
